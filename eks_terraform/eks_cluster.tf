
# EKS cluster creation, change version if needed in vars.tf and also cluster name can be changed
# Relying on some external modules to help with the EKS cluster
module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  cluster_name    = var.cluster_name
  cluster_version = var.kubernetes_version
  subnets         = [aws_subnet.myVPC-private-1a.id,aws_subnet.myVPC-private-1b.id,aws_subnet.myVPC-private-1c.id]

  tags = {
    Environment = "UAT"
   
  }

  vpc_id = aws_vpc.myVPC.id

  workers_group_defaults = {
    root_volume_type = "gp2"
  }
# managed node groups
  worker_groups = [
    {
      name                          = "worker-group-1"
      instance_type                 = "t2.small"
      additional_userdata           = "echo foo bar"
      asg_desired_capacity          = 2
      additional_security_group_ids = [aws_security_group.worker_group_mgmt_one.id]
    },
    {
      name                          = "worker-group-2"
      instance_type                 = "t2.medium"
      additional_userdata           = "echo foo bar"
      additional_security_group_ids = [aws_security_group.worker_group_mgmt_two.id]
      asg_desired_capacity          = 1
    },
  ]
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}