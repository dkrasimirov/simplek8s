variable "RDS_PASSWORD" {
  type        = string
  sensitive   = true
}


variable "MARIADB_VERSION" {
  default = "10.5.12"

}

variable "cluster_name" {
   default =  "demo-eks-cluster01"

}

variable "kubernetes_version" {
  default = "1.20"
}