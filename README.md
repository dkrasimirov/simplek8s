# Solution diagram

![Solution Diagram](images/eksdiagram.jpg)

The application is the same as in the project https://gitlab.com/dkrasimirov/niceproject where most of the functionality has been explained in the detail.
In short this application in this case is packed in a Docker container and then deployed as a pod in EKS (Elastic Kubernetes Services). EKS is managed service and can be deployed easy with terraform. On the diagram hs been shown that the solution creates similarly to the other project separate VPC with three Public and three private subnets in three different availability zones in eu-central-1 region. Then what happen is that the actual "worker" nodes of the EKS cluster where the pods are running(pod is like a container but there are also in certain situations multi container pods https://kubernetes.io/docs/concepts/workloads/pods/). In this deployment it is used "classic LB" for simplicity but it could be used ALB usually in more complex deployments using ALB as ingress controller. The Loadbalancer is deployed in the Public subnets and the worker nodes in the private subnets. Again communication is done through NAT gateway for the private subnets and internet gateway for the public subnets. The pods could be one or many (replicas in k8s) and they depends on secrets in kubernetes and everything is deployed with one yaml file. The solution rely on external RDS instance and the pods store and query the same (Hello from GrayLog) from there.
As for demonstration purpose two of the worker nodes are created in one ASG and one other in the other ASG. The EKS is using auto-scaling groups for worker nodes but in one auto-scaling group the worker nodes has to be from the same size like t2.micro. So in that case one of the node has been created with different size.

# Dockerfile.

The dockerfile is used and the Build functionality of gitlab. This way the docker container is build with "latest" tag and stored in a container registry in Gitlab. 
In the Dockerfile two stage build process is used. Fist from line 1 to line 10 based on the official golang docker image, it downloads the dependencies and creates intermediate container copying all files and create a binary package inside. Second stage is from from golang:1.17.5-alpine3.15 line 11 to the end. The arguments for username, password and RDS endpoint are passed as docker arguments and are made globally for the container as environment variable. The second stage build is used only to copy already created binary in the first stage and then create an image only with it. The EXPOSE line means that by default the application will listen on port 8080.

# Description of the terraform files used

To deploy EKS all the terraform files are stored in "eks_terraform". 
- vpc.tf - deploying the VPC myVPC with three private eu-central-1a/1b/1c and three public eu-central-1a/1b/1c.
From line 1 to line 26 is the VPC creation. From line 26 to line 68 -  public subnets creation (3 in 3 different AZ). From line 68 to line 107 it creates three private subnets again in different AZ. Line 108 to 116 is for resource "aws_internet_gateway" which is the internet Gateway needed for the public subnet to communicate to internet. From line 117 to line 129 - the routing table (resource "aws_route_table" ) which associates with the internet gateway - gateway_id = aws_internet_gateway.myVPC-gw.id. Then from line 130 on, this is the association between a public routing table and public subnets. Also 0.0.0.0/0 is the route added with in igw (internet gateway) meaning use it as default gateway.
One important fact here is to assign specific tags to the subnets. This is requirement so EKS can function correctly. The tags has to be applied for all of the subnets.
```
tags = {
    Name = "myVPC-private-1c"
     "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"             = "1"
  }

   tags = {
    Name = "myVPC-public-1c"
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                      = "1"
  }

```
- version.tf - this files sets verion terraform version >= 0.12.
- vars.tf - For the EKS cluster name, RDS password and Mariadb version.
- securitygroup.tf - It ensures the connectivity on port 22 and this is only applicable when there is a need to ssh directly to the worker nodes. Because the worker nodes are deployed on the private subnets then there as a need of some EC2 jump server if you want to ssh to the worker nodes. Normally that is not needed and the whole interraction with the cluster is done through "kubectl" and  the kubernetes API.
- rds.tf - This is the RDS (mariadb) service. Line 1 to 17 is the actual resource for subnet group and parameters group with default parameters except specified. The RDS is deployed in subnet_ids  = [aws_subnet.myVPC-private-1a.id, aws_subnet.myVPC-private-1b.id] in fact two private subnets from myVPC. Can be deployed in three in case of eu-central-1.
From line 18 "aws_db_instance" "mariadb" - this is the actual DB instance in RDS.
The password is delivered from the varible mentioned. Also to improve the resiliency the "multi_az" can be set to "true". Also in other situations "instance_class" can be changed to bigger instance but for this particular case t2.micro is enough.
- outputs.tf - the output from terraform apply. Goot to have in this case the RDS enpoint which is needed for kubernetes yaml deployment file.
- nat.tf - The nat gateeway and the routing tables for the private subnets.
- kubernetes.tf - is specific file so EKS can complete sucessfully , please see commands inside it but basically this has been taken bu the recommendations related to terraform.
- eks_cluster.tf - This is the main code responsible for deployment of EKS. The cluster_version = "1.20" is exactly what kubernetes version needs to be used. In the following web page AWS has listed the supported versions. The value can be changed.
The deployment of EKS rely on external modules as well.
https://docs.aws.amazon.com/eks/latest/userguide/kubernetes-versions.html
Worker groups are ASG node pools with different or the same size of the instances if needed. This is the way to define one or more worker groups(managed node groups).



| Security Group  | inbound | outbound |
| -------------   | --------| ---------|
| worker_group_mgmt_one | 22 from 10.0.0.0/8 | any |
| worker_group_mgmt_two | 22 from 10.0.0.0/8  | any |
| all_worker_mgmt  | 22 from private subnet ranges | any |
| allow-mariadb  | 3306 from any | any |



## Step 1 deploy the EKS cluster and RDS instance

- Go to the folder eks_terraform and do the following:

```
terraform init
terraform validate
terraform plan
terraform apply

```


On the terraform apply step it will ask for the RDS password for the root user, please store this password in a vault or keepass solution.

- terrafrom init - this step download the modules as well

```
terraform init
Initializing modules...
Downloading terraform-aws-modules/eks/aws 17.24.0 for eks...
- eks in .terraform/modules/eks
- eks.fargate in .terraform/modules/eks/modules/fargate
- eks.node_groups in .terraform/modules/eks/modules/node_groups

Initializing the backend...

Initializing provider plugins...
- Finding hashicorp/random versions matching "3.1.0"...
- Finding hashicorp/local versions matching ">= 1.4.0, 2.1.0"...
- Finding hashicorp/null versions matching "3.1.0"...
- Finding hashicorp/kubernetes versions matching ">= 1.11.1, >= 2.0.1"...
- Finding hashicorp/cloudinit versions matching ">= 2.0.0"...
- Finding terraform-aws-modules/http versions matching ">= 2.4.1"...
- Finding hashicorp/aws versions matching ">= 3.20.0, >= 3.56.0"...
- Installing hashicorp/kubernetes v2.7.1...
- Installed hashicorp/kubernetes v2.7.1 (signed by HashiCorp)
- Installing hashicorp/cloudinit v2.2.0...
- Installed hashicorp/cloudinit v2.2.0 (signed by HashiCorp)
- Installing terraform-aws-modules/http v2.4.1...
- Installed terraform-aws-modules/http v2.4.1 (self-signed, key ID B2C1C0641B6B0EB7)
- Installing hashicorp/aws v3.70.0...
- Installed hashicorp/aws v3.70.0 (signed by HashiCorp)
- Installing hashicorp/random v3.1.0...
- Installed hashicorp/random v3.1.0 (signed by HashiCorp)
- Installing hashicorp/local v2.1.0...
- Installed hashicorp/local v2.1.0 (signed by HashiCorp)
- Installing hashicorp/null v3.1.0...
- Installed hashicorp/null v3.1.0 (signed by HashiCorp)

Partner and community providers are signed by their developers.
If you'd like to know more about provider signing, you can read about it here:
https://www.terraform.io/docs/cli/plugins/signing.html

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```


- terraform validate 
```
terraform validate
Success! The configuration is valid.

```

- terrafrom plan

```
terraform plan
var.RDS_PASSWORD
  Enter a value:

  
Plan: 54 to add, 0 to change, 0 to destroy.

Changes to Outputs:
  + RDS                       = (known after apply)
  + cluster_endpoint          = (known after apply)
  + cluster_id                = (known after apply)
  + cluster_name              = "demo-eks-cluster01"
  + cluster_security_group_id = (known after apply)

```
- terraform apply

```
var.RDS_PASSWORD
  Enter a value:

Plan: 54 to add, 0 to change, 0 to destroy.

Changes to Outputs:
  + RDS                       = (known after apply)
  + cluster_endpoint          = (known after apply)
  + cluster_id                = (known after apply)
  + cluster_name              = "demo-eks-cluster01"
  + cluster_security_group_id = (known after apply)

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes


  Apply complete! Resources: 1 added, 0 changed, 0 destroyed.

Outputs:

RDS = "mariadb.xxxxxxxx.eu-central-1.rds.amazonaws.com:3306"
cluster_endpoint = "https://XXXXXXXXXXXXXXXX.gr7.eu-central-1.eks.amazonaws.com"
cluster_id = "demo-eks-cluster01"
cluster_name = "demo-eks-cluster01"
cluster_security_group_id = "sg-xxxxxxxxxxxx"

```
NOTE: if terraform apply did not finish sucessfully and with the output simmilar to the one below do terraform apply command again.

```
Error: Error making request: Get "https://xxxxxxxxxxxxxxxxxx.gr7.eu-central-1.eks.amazonaws.com/healthz": dial tcp <some public ip address>:443: connect: connection timed out
│ 
│   with module.eks.data.http.wait_for_cluster[0],
│   on .terraform/modules/eks/data.tf line 92, in data "http" "wait_for_cluster":
│   92: data "http" "wait_for_cluster" {
```


It will take time to deploy fulle EKS and RDS instance.

## Step 2 install kubectl
You can follow this guide to install kubectl to the local desktop machine Windows, MAC or Linux.
https://kubernetes.io/docs/tasks/tools/

After the terraform apply is finished please note the output and note in text editor the RDS endpoint. You can also find the endpoint in the RDS section in AWS management console.
After that, you can try the command below as replacing the region if it is not eu-central-1.
```
aws eks --region eu-central-1 update-kubeconfig --name $(terraform output -raw cluster_name)
```
After this command the .kube/config file is prepared and stored in your home folder, adding kubernetes admin user, certificate, kubernetes API and then you can interract to EKS.



```
kubectl get nodes -o wide

NAME                                          STATUS   ROLES    AGE     VERSION               INTERNAL-IP   EXTERNAL-IP   OS-IMAGE         KERNEL-VERSION                CONTAINER-RUNTIME
ip-10-0-5-169.eu-central-1.compute.internal   Ready    <none>   3m45s   v1.21.5-eks-bc4871b   10.0.5.169    <none>        Amazon Linux 2   5.4.156-83.273.amzn2.x86_64   docker://20.10.7
ip-10-0-5-33.eu-central-1.compute.internal    Ready    <none>   3m46s   v1.21.5-eks-bc4871b   10.0.5.33     <none>        Amazon Linux 2   5.4.156-83.273.amzn2.x86_64   docker://20.10.7
ip-10-0-6-7.eu-central-1.compute.internal     Ready    <none>   3m45s   v1.21.5-eks-bc4871b   10.0.6.7      <none>        Amazon Linux 2   5.4.156-83.273.amzn2.x86_64   docker://20.10.7
```

Then please verify the kube-system namespace containing the system pods. If something goes wrong here the cluster has not been correctly provisioned.

The output should be simmilar to the following one.

```
kubectl get pods -n kube-system
NAME                      READY   STATUS    RESTARTS   AGE
aws-node-4vdp6            1/1     Running   0          4m34s
aws-node-psxr4            1/1     Running   0          4m26s
aws-node-rsf7f            1/1     Running   0          4m45s
coredns-85cc4f6d5-s5jfh   1/1     Running   0          7m51s
coredns-85cc4f6d5-z4fx5   1/1     Running   0          7m51s
kube-proxy-5gps7          1/1     Running   0          4m26s
kube-proxy-m6bln          1/1     Running   0          4m45s
kube-proxy-zs8sn          1/1     Running   0          4m34s
```

## Step 3 apply the kubernetes-deployment.yaml

Change the deployment yaml file "kubernetes-deployment.yaml"
- Add the RDS endpoint in line 14 (there as a snippet blow)

```
metadata:
  name: mysql
  namespace: test
spec:
  type: ExternalName
  externalName: "mariadb.xxxxxxxxxxxxx.eu-central-1.rds.amazonaws.com" ## change this to match to the name 
```

- Add the password provided by terraform apply for the RDS instance in line 26 as in the snippet below.

```
type: Opaque
data:
  # Output of echo -n 'whateverPassword' | base64 
  db-password: xxxxxxxxx
```
to get the correct value type for example if password is "password" and paste the encoded string in db-password:<encodedStringbase64>

```
echo -n password | base64 
```

- Apply the manifest to the EKS cluster

```
kubectl apply -f kubernetes-deployment.yaml
```
- verify the output with the following command. The output in your case could be slightly different. 

```
kubectl get all -n test
NAME                       READY   STATUS    RESTARTS   AGE
pod/goapp-f45f6f77-mt7x7   1/1     Running   0          48s

NAME            TYPE           CLUSTER-IP       EXTERNAL-IP                                                                  PORT(S)        AGE
service/goapp   LoadBalancer   172.20.157.101   xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.eu-central-1.elb.amazonaws.com   80:32583/TCP   48s
service/mysql   ExternalName   <none>           mariadb.xxxxxxxxxxxx.eu-central-1.rds.amazonaws.com                          <none>         48s

NAME                    READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/goapp   1/1     1            1           48s

NAME                             DESIRED   CURRENT   READY   AGE
replicaset.apps/goapp-f45f6f77   1         1         1       48s
```

to verify the pod is running
```
kubectl get pods -n test

goapp-f45f6f77-mt7x7   1/1     Running   0          2m51s

```
to get the loadbalancer  external url

```
kubectl get svc -n test

NAME    TYPE           CLUSTER-IP       EXTERNAL-IP                                                                  PORT(S)        AGE
goapp   LoadBalancer   172.20.157.101   xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.eu-central-1.elb.amazonaws.com   80:32583/TCP   3m54s
mysql   ExternalName   <none>           mariadb.xxxxxxxxxxxxx.eu-central-1.rds.amazonaws.com                          <none>         3m54s
```



You can see the external DNS name for the loadbalancer which was created for the service goapp

The loadbalancer can be verified also through AWS management console in EC2 -> Load Balancers -> DNS name

- Open web browser and paste the URL. If it gets time out on the first try wait a bit and refresh the web page, the application will output the string (Hello from Graylog).

forgot to mention to scale the application to run in more pods you have to apply the following below. 
```
kubectl scale deployment goapp --replicas=3

or in tha yaml file  line 38 change replicas=3 and then again do kubectl apply

spec:
  replicas: 3

```


# Delete the deployment and clean up

```
kubectl delete -f kubernetes-deployment.yaml

```

to delete EKS

```
terraform destroy
```
